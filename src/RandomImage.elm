module RandomImage exposing (..)

import AlbumCommon exposing (tagEditorUrl)
import Requests exposing (checkHttpAttempt)

import Html.Styled exposing (..)
import Html.Styled.Attributes exposing (src, css, height)
import Html.Styled
import Http
import FileList exposing (FileList, FileListSource, FileListResponse)
import Json.Decode
import Browser
import Browser.Navigation as Navigation
import Urls
import Random
import Style
import Browser.Events
import Css.Global
import Task
import Browser.Dom as Dom

-- Model

type State
    = NoList
    | HasList Int Int
    | HasFile Int Int
    | NoServer

type alias Model =
    { state: State
    , windowSize: (Int, Int)
    }

init : flags -> ( Model, Cmd Msg )
init _ =
    ( Model NoList (1920, 1080)
    , Cmd.batch
        [ requestList
        , Task.perform
            (\vp -> WindowResized (round vp.viewport.width) (round vp.viewport.height))
            Dom.getViewport
        ]
    )


-- Msg

type Msg
    = FileListCreated Int Int
    | ImageSelected Int Int
    | NetworkError
    | WindowResized Int Int




--Update


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        FileListCreated listId listLength ->
            let
                model_ = {model | state = HasList listId listLength}

                cmd = Random.generate (ImageSelected listId) <| Random.int 0 (listLength - 1)
            in
            (model_, cmd)
        ImageSelected listId fileId ->
            ({model | state = HasFile listId fileId}, Cmd.none)
        NetworkError ->
            ({model | state = NoServer}, Cmd.none)
        WindowResized sizeX sizeY ->
            ({model | windowSize = (sizeX, sizeY)}, Cmd.none)


checkHttpAttempt : (a -> Msg) -> Result Http.Error a -> Msg
checkHttpAttempt func res=
    case res of
        Ok val ->
            func val
        Err e ->
            NetworkError


requestList : Cmd Msg
requestList =
    Http.send
        (checkHttpAttempt (\val -> FileListCreated val.id val.length))
        (Http.get (Urls.searchUrl "of") FileList.fileListDecoder)

--Subscriptions


subscriptions : Model -> Sub Msg
subscriptions model =
    Browser.Events.onResize WindowResized



-- View

view : Model -> Html Msg
view model =
    let
        content = case model.state of
            NoList ->
                div [] [text "No list"]
            HasList _ _ ->
                div [] [text "Has list"]
            HasFile list file ->
                img
                    [ src <| Urls.fileListGetFileUrl list file
                    , css [Style.fitImageToContent <| Tuple.second model.windowSize]
                    ]
                    []
            NoServer ->
                div [] [text "No connection"]
    in
        div
            [ height <| Tuple.second model.windowSize
            ]
            [ Css.Global.global Style.globalStyle
            , content
            ]


--Main

main : Program () Model Msg
main =
    Browser.document
        { init = init
        , update = update
        , view = (\model -> Browser.Document "Flash album" [toUnstyled <| view model])
        , subscriptions = subscriptions
        }



