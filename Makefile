all: tageditor album random

tageditor:
	elm make src/TagEditor.elm --output=output/tag_editor.html

album:
	elm make src/Main.elm --output=output/album.html

random:
	elm make src/RandomImage.elm --output=output/randomimage.html



clean:
	rm -r elm-stuff
